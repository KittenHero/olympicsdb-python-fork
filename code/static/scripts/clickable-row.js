$(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
	$(".clickable-row").mouseover(function() {
        $(this).css('background-color', '#eeeeee');
    });
	$(".clickable-row").mouseout(function() {
        $(this).css('background-color', 'white');
    });

});